﻿using Sol_LoginTask.Entity.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_LoginTask.Entity
{
    public class UserLoginEntity: IUserLoginEntity
    {
        public decimal UserId { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }
    }
}
