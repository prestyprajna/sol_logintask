﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_LoginTask.Entity.Interface
{
    public interface IUserLoginEntity
    {
         decimal UserId { get; set; }

         string Username { get; set; }

         string Password { get; set; }
    }
}
