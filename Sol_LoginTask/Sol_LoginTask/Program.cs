﻿using Sol_LoginTask.Entity;
using Sol_LoginTask.Repository;
using Sol_LoginTask.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_LoginTask
{
    class Program
    {
        static void Main(string[] args)
        {
            Task.Run(async () =>
            {
                try
                {
                    bool flag = await new UserRepository().LoginAsync(new UserLoginEntity()
                    {
                        Username = "Diwik",
                        Password = "Diwik123"
                    });                    

                    string msg = (flag == true) ? "Login successfull" : "Login not successfull";
                    Console.WriteLine(msg);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }           


            }).Wait();


        }
    }
}
