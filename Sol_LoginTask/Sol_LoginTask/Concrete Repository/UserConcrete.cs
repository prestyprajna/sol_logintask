﻿using Sol_LoginTask.Concrete_Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sol_LoginTask.Entity.Interface;
using Sol_LoginTask.ORD;
using Sol_LoginTask.Common_Repository;

namespace Sol_LoginTask.Concrete_Repository
{
    public class UserConcrete : IUserConcrete
    {
        #region   declaration

        private LoginInfoDataContext dc = null;

        #endregion

        #region   constructor

        public UserConcrete()
        {
            dc = new LoginInfoDataContext();
        }

        #endregion

        #region   public methods

        public async Task<bool> LoginAsync(string command,IUserLoginEntity entityObj)
        {
            try
            {
                int? status = null;
                String message = null;

                return await Task.Run<bool>(() =>
                {
                    var getQuery =
                    dc
                    ?.uspUserLogin(
                        command,
                        entityObj?.Username,
                        entityObj?.Password,
                        ref status,
                        ref message
                        );

                    return (status == 1) ? true : false;
                });
            }
            catch (Exception)
            {

                throw;
            }
           
        }
       

        #endregion

    }
}
