﻿using Sol_LoginTask.Repository.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sol_LoginTask.Entity.Interface;
using Sol_LoginTask.Concrete_Repository.Interface;
using Sol_LoginTask.Concrete_Repository;

namespace Sol_LoginTask.Repository
{
    public class UserRepository : IUserRepository
    {
        #region  declaration

        private IUserConcrete userConcreteObj = null;

        #endregion

        #region  constructor

        public UserRepository()
        {
            userConcreteObj = new UserConcrete();
        }

        #endregion

        #region  public methods

        public async Task<bool> LoginAsync(IUserLoginEntity entityObj)
        {
            try
            {
                return await userConcreteObj?.LoginAsync("ValidLogin", entityObj);
            }
            catch (Exception)
            {

                throw;
            }
                  
        }

        #endregion

    }
}
