﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_LoginTask.Common_Repository
{
    public interface IGetDelegate<TEntity> where TEntity : class
    {
        Task<bool> LoginAsync(string command, TEntity entityObj);
    }
}


