﻿CREATE PROCEDURE uspUserLogin
(
	@Command varchar(50)=NULL,	
	@Username Varchar(50)=NULL,
	@Password varchar(50)=NULL,

	@Status int=NULL out,
	@Message Varchar(50)=NULL out

)
AS
	BEGIN

	--declaration of variables
	DECLARE @tempUserId Numeric(18,0)
	

		IF @Command='ValidLogin'
		BEGIN	

			SELECT @tempUserId=LI.UserId
				FROM loginInfo AS LI
					WHERE LI.Username=@Username AND LI.Password=@Password


			IF @tempUserId IS NOT NULL
			BEGIN
				SET @Status=1
				SET @Message='LOGIN SUCCESSFULL'
			END
			ELSE 
			BEGIN
				SET @Status=0
				SET @Message='LOGIN NOT SUCCESSFULL'
			END	

		END		

	END
